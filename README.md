# README #

Shows usage of Realm with Android RecyclerView using RealmRecyclerViewAdapter library.

Based on Realm's own example in the library 
https://github.com/realm/realm-android-adapters, but instead of compiling library 'in project' uses dependency.

Also shows usage of primary key for Realm objects, which puzzled me for a while...